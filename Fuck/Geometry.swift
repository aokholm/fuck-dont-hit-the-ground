//
//  geometry.swift
//  Fuck
//
//  Created by Andreas Okholm on 08/11/14.
//  Copyright (c) 2014 Vaavud. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

import CoreLocation
import MapKit

struct Line {
    var a : Double
    var b : Double
    var c : Double
}

func createLineFromTwoPoints(#point1: MKMapPoint, #point2: MKMapPoint) -> Line {
    
    //        var line = Line()
    
    
    var a = point2.y - point1.y
    var b = point1.x - point2.x
    var c = a*point1.x+b*point1.y
    
    return Line(a: a, b: b, c: c)
    
}

func createLineFromPointAndHeading(#point: MKMapPoint, #heading: Double) -> Line {
    
    var headingRad = heading/180 * M_PI
    
    var point2 = MKMapPoint(x: point.x + sin(headingRad), y: point.y - cos(headingRad))  // minus cosinus since y axis is inverted.
    
    return createLineFromTwoPoints(point1: point, point2: point2)
    
}

func findIntersectionPoint (#line1: Line, #line2: Line) -> MKMapPoint? {
    
    var det = line1.a*line2.b - line2.a*line1.b
    if(det == 0){
        //Lines are parallel
        return (nil)
    }else{
        var x = (line2.b*line1.c - line1.b*line2.c)/det
        var y = (line1.a*line2.c - line2.a*line1.c)/det
        return (MKMapPoint(x: x, y: y))
    }
}


func checkedIntersectionPoint (#startingPoint: MKMapPoint, #course: Double, #lineEnd1: MKMapPoint, #lineEnd2: MKMapPoint) -> MKMapPoint?{
    
    var lineSegment = createLineFromTwoPoints(point1: lineEnd1, point2: lineEnd2)
    
    var lineCourse = createLineFromPointAndHeading(point: startingPoint, heading: course)
    
    var intersectionPoint = findIntersectionPoint(line1: lineSegment, line2: lineCourse)
    
    if (intersectionPoint != nil) {
        // check if intersection is in the course direction
        
        var xDist = (intersectionPoint!.x - startingPoint.x)
        var yDist = (intersectionPoint!.y - startingPoint.y)
        
        
        var lengthToPoint = sqrt(pow(xDist,2) + pow(yDist, 2))
        
        var dirX = xDist/lengthToPoint
        var dirY = yDist/lengthToPoint
        
        var courseRad = course/180*M_PI
        
        // should be 1 if they point in the same direction
        // -1 if they point in the opposite direction
        if ((sin(courseRad)*dirX - cos(courseRad)*dirY) < 0.9 ) { // minus cos since y axis is inverted.
            return nil
        }
        
        // check if the intersection point is between the points on the end
        if (abs(lineEnd2.x - lineEnd1.x) > abs(lineEnd2.y - lineEnd1.y)) {
            if (intersectionPoint!.x < min(lineEnd1.x, lineEnd2.x) || intersectionPoint!.x > max(lineEnd1.x, lineEnd2.x)) {
                return nil
            }
        } else {
            if (intersectionPoint!.y < min(lineEnd1.y, lineEnd2.y) || intersectionPoint!.y > max(lineEnd1.y, lineEnd2.y)) {
                return nil
            }
        }
        
        
        return intersectionPoint
    }
    
    return nil
}