//
//  ViewController.swift
//  Fuck
//
//  Created by Andreas Okholm on 08/11/14.
//  Copyright (c) 2014 Vaavud. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!

    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationStatus : NSString = "Not Started"
    var userLocation : CLLocation!
    var badAreaLocationss : [[CLLocationCoordinate2D]] = []
    var areas : [Area] = []
    var annotationIntersection : MKPointAnnotation!
    var editableAreaIndex : Int?
    var areaInsertIndex : Int?
    
    @IBOutlet weak var labelCourse: UILabel!
    @IBOutlet weak var labelSpeed: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelTime: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initLocationManager()
        
        mapView.delegate = self
        mapView.mapType = MKMapType.Satellite
        mapView.showsUserLocation = true
        
    }
    
    func createMapAnnotation() {
        // 2
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegion(center: userLocation.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
        
        annotationIntersection = MKPointAnnotation()
        annotationIntersection.coordinate = userLocation.coordinate
        annotationIntersection.title = "Fuck Point"
        annotationIntersection.subtitle = "this is where you will be going down"
    }
    
    @IBAction func handleMapTap(gestureRecognizer: UITapGestureRecognizer) {
        
        
        if (gestureRecognizer.state != UIGestureRecognizerState.Ended)  {
            return
        }
        
        let touchPoint = gestureRecognizer.locationInView(self.mapView)
        let touchMapCoordinate = mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        let touchMapPoint = MKMapPointForCoordinate(touchMapCoordinate)
        
        
        for index in 0...areas.count-1 {
            if areas[index].containsPoint(touchMapPoint) {
                showAreaActionSheet(index)
            }
        }
    }
    
    
    func showAreaActionSheet(areaIndex : Int) {
        let alert = UIAlertController(title: "selected Area", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let cancelAction = UIAlertAction(title: "cancel", style: UIAlertActionStyle.Cancel) {
            (alertAction : UIAlertAction!) in
            // dont do anything
        }
        
        
        let deleteAction = UIAlertAction(title: "delete", style: UIAlertActionStyle.Destructive) {
            (alertAction : UIAlertAction!) in
            
            
            let confirmDeleteAlert = UIAlertController(title: "selected Area", message: "Are you sure you want to delete the area?", preferredStyle: UIAlertControllerStyle.Alert)
            
            let deleteAction = UIAlertAction(title: "delete", style: UIAlertActionStyle.Destructive) {
                (alertAction : UIAlertAction!) in
            
                self.areas[areaIndex].removeAnnotationFromMap()
                self.areas.removeAtIndex(areaIndex)
            }
            
            
            confirmDeleteAlert.addAction(deleteAction)
            confirmDeleteAlert.addAction(cancelAction)
            self.presentViewController(confirmDeleteAlert, animated: true, completion: nil)

            
        }
        
        var lockAreaAction : UIAlertAction
        
        if self.areas[areaIndex].locked {
            lockAreaAction = UIAlertAction(title: "edit area", style: UIAlertActionStyle.Default) {
                (alertAction : UIAlertAction!) in
                self.areas[areaIndex].unlock()
            }
        } else {
            lockAreaAction = UIAlertAction(title: "lock area", style: UIAlertActionStyle.Default) {
                (alertAction : UIAlertAction!) in
                self.areas[areaIndex].lock()
            }
        }
        
        
        alert.addAction(deleteAction)
        alert.addAction(lockAreaAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func handleMapLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        
        println(gestureRecognizer);
        
        if (gestureRecognizer.state != UIGestureRecognizerState.Ended)  {
            return
        }
        
        let touchPoint = gestureRecognizer.locationInView(self.mapView)
        let touchMapCoordinate = mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        
        if areas.count == 0 {
            areas.append(Area())
            areas.last!.mapView = mapView
        }
        
        if (areas.last!.locked ) {
            areas.append(Area())
            areas.last?.mapView = mapView
        }
        
        areas.last?.addCorner(touchMapCoordinate)
        
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        
        if overlay is MKPolygon {
            var polygonRenderer = MKPolygonRenderer(overlay: overlay)
            polygonRenderer.strokeColor = UIColor.blueColor()
            polygonRenderer.fillColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.2)
            polygonRenderer.lineWidth = 5
            return polygonRenderer
        }
        
        return nil
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if (annotation.isKindOfClass(MKUserLocation)) {
            return nil
        }
        
        if let corner = annotation as? Corner {
            
            var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("CustomPinAnnotationView") as? MKPinAnnotationView
 
            if (pinView == nil) {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier:"CustomPinAnnotationView" )
                pinView!.pinColor = MKPinAnnotationColor.Green
                pinView!.animatesDrop = true
                pinView!.canShowCallout = true
                pinView!.draggable = true
                
                
                let rightButton = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as! UIButton
                rightButton.addTarget(nil, action: nil, forControlEvents: UIControlEvents.TouchUpInside)
                pinView!.rightCalloutAccessoryView = rightButton
                
            } else {
                pinView!.annotation = annotation
            }
            
//            if corner.getIndex() == 0 {
//                pinView?.pinColor = MKPinAnnotationColor.Red
//            }
            
            return pinView
        }
        
//        if annotation.isKindOfClass(MKPointAnnotation) {
//            var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier("PinAnnotationView") as? MKPinAnnotationView
//            
//            if (pinView == nil) {
//                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier:"PinAnnotationView" )
//            } else {
//                pinView!.annotation = annotation
//            }
//
//        }
        
        return nil
    }
    
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        
        if let corner = view.annotation as? Corner {
            if newState == MKAnnotationViewDragState.Ending {
                corner.area.updateOverlay()
            }
        }
        
    }
    
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        
        let alert = UIAlertController(title: "Edit Corner", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let deleteAction = UIAlertAction(title: "delete", style: UIAlertActionStyle.Destructive) {
            (alertAction : UIAlertAction!) in
            
            if let corner = view.annotation as? Corner {
                corner.area.removeCorner(corner)
            }
        }
        
        let addBeforeAction = UIAlertAction(title: "add point before", style: UIAlertActionStyle.Default) {
            (alertAction : UIAlertAction!) in
            // SET some view controller state
            if let corner = view.annotation as? Corner {
                corner.area.insertIndex = corner.getIndex()
            }
            
        }
        
        let addAfterAction = UIAlertAction(title: "add point after", style: UIAlertActionStyle.Default) {
            (alertAction : UIAlertAction!) in
            // SET some view controller state
            if let corner = view.annotation as? Corner {
                corner.area.insertIndex = corner.getIndex()+1
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "cancel", style: UIAlertActionStyle.Cancel) {
            (alertAction : UIAlertAction!) in
            // dont do anything
        }
        
        
        alert.addAction(deleteAction)
        alert.addAction(addBeforeAction)
        alert.addAction(addAfterAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        switch UIDevice.currentDevice().systemVersion.compare("8.0.0", options: NSStringCompareOptions.NumericSearch) {
        case .OrderedSame, .OrderedDescending:
            locationManager.requestWhenInUseAuthorization()
        case .OrderedAscending:
            println("iOS < 8.0")
        }
        locationManager.startUpdatingLocation()
    }
    
    
    func checkTimeToCollision(location: CLLocation) {

        var minDistance : Double = -1;

        var mpLocation = MKMapPointForCoordinate(location.coordinate)
        
        for area in areas {
            
            // check if inside polygon
            
            if area.containsPoint(mpLocation) {
                println("Position inside area");
            }
            
            for (var corner1index = 0; corner1index < area.corners.count; corner1index++) {
                var corner2index = (corner1index+1)%area.corners.count;
                
                var interSectionPoint = checkedIntersectionPoint(
                    startingPoint: mpLocation,
                    course: location.course,
                    lineEnd1: MKMapPointForCoordinate(area.corners[corner1index].coordinate),
                    lineEnd2: MKMapPointForCoordinate(area.corners[corner2index].coordinate))
                
                if (interSectionPoint != nil) {
                    
                    var dist = MKMetersBetweenMapPoints(mpLocation, interSectionPoint!)
                    
                    if (dist < minDistance || minDistance < 0) {
                        minDistance = dist
                        annotationIntersection.coordinate = MKCoordinateForMapPoint(interSectionPoint!)
                    }
                }
                
            }
        }
        
        var timeToFuck : Double = 0;
        
        if (minDistance > 0) {
            mapView.addAnnotation(annotationIntersection)
            timeToFuck = minDistance / location.speed;
        } else {
            mapView.removeAnnotation(annotationIntersection)
        }
        
        labelDistance.text = NSString(format:"%.0f", minDistance) as String;
        labelCourse.text = NSString(format:"%.0f", location.course) as String;
        labelSpeed.text = NSString(format: "%.1f", location.speed) as String;
        labelTime.text = NSString(format: "%.1f", timeToFuck) as String;
        
    }
    

    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {

                var locationArray = locations as NSArray
        userLocation = locationArray.lastObject as! CLLocation

//        println("lat: \(userLocation.coordinate.latitude) log: \(userLocation.coordinate.longitude) speed: \(userLocation.speed) course: \(userLocation.course)")
        
        if (annotationIntersection == nil ) {
            createMapAnnotation()
        }
        
        checkTimeToCollision(userLocation); // also updates location of intersection point
    }

    
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        locationManager.stopUpdatingLocation()
        if ((error) != nil) {
            if (seenError == false) {
                seenError = true
                print(error)
            }
        }
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager!,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
            NSNotificationCenter.defaultCenter().postNotificationName("LabelHasbeenUpdated", object: nil)
            if (shouldIAllow == true) {
                NSLog("Location to Allowed")
                // Start location services
                locationManager.startUpdatingLocation()
            } else {
                NSLog("Denied access: \(locationStatus)")
            }
    }
    
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager!) -> Bool {
        return true
    }
    
    
    
    
    
    
}

