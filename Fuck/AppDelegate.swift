//
//  AppDelegate.swift
//  Fuck
//
//  Created by Andreas Okholm on 08/11/14.
//  Copyright (c) 2014 Vaavud. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        let credentialsProvider = AWSCognitoCredentialsProvider.credentialsWithRegionType(
            AWSRegionType.EUWest1,
//            accountId: cognitoAccountId,
//            identityPoolId: cognitoIdentityPoolId,
//            unauthRoleArn: cognitoUnauthRoleArn,
//            authRoleArn: cognitoAuthRoleArn)
        accountId: "129518110474",
        identityPoolId: "eu-west-1:58c7dfa5-864b-48d9-9ea4-37b6980ec2b1",
        unauthRoleArn: "arn:aws:iam::129518110474:role/Cognito_agroundUnauth_DefaultRole",
        authRoleArn: "arn:aws:iam::129518110474:role/Cognito_agroundAuth_DefaultRole")
        
        let defaultServiceConfiguration = AWSServiceConfiguration(
            region: AWSRegionType.EUWest1,
            credentialsProvider: credentialsProvider)
        AWSServiceManager.defaultServiceManager().setDefaultServiceConfiguration(defaultServiceConfiguration)
        
        AWSLogger.defaultLogger().logLevel = .Error
        
        
        let syncClient = AWSCognito.defaultCognito()
        
        let dataset = syncClient.openOrCreateDataset("myDataset")
//        dataset.setString("awesome", forKey: "firstKey")
        
        dataset.synchronize()
        
        let storedvalue = dataset.stringForKey("firstKey")
        
        println("stored value \(storedvalue)")
        
        
        let dynamoDB = AWSDynamoDB.defaultDynamoDB()
        let listTableInput = AWSDynamoDBListTablesInput()
        dynamoDB.listTables(listTableInput).continueWithBlock{ (task: BFTask!) -> AnyObject! in
            if (task.error != nil) {
                println("error getting listOutput, message: \(task.error.description)")
            }
            
            let listTablesOutput = task.result as? AWSDynamoDBListTablesOutput
            
            if listTablesOutput != nil {
                for tableName : AnyObject in listTablesOutput!.tableNames {
                    println("\(tableName)")
                }

            }
            return nil
        }
        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

