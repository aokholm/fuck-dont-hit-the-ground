//
//  Area.swift
//  Fuck
//
//  Created by Andreas Okholm on 26/12/14.
//  Copyright (c) 2014 Vaavud. All rights reserved.
//

import Foundation
import MapKit

class Area {
    var corners : [Corner] = []
    var polygon : MKPolygon?
    weak var mapView : MKMapView?
    var polyPath : CGPathRef?
    var locked : Bool = false
    var insertIndex : Int?
    
    // should be auto generated but apprently does for some reason not work.
    init() {
        
    }
    
    func addCorner(coordinate: CLLocationCoordinate2D){
        let corner = Corner(coordinate: coordinate, area: self)
        
        if (insertIndex != nil) {
            corners.insert(corner, atIndex: insertIndex!)
            insertIndex = nil
        } else {
            corners.append(corner)
        }
        
        updateTitles()
        
        mapView?.addAnnotation(corner);
        
        self.updateOverlay()
    }
    
    func removeCorner(corner: Corner) {
        
        mapView?.removeAnnotation(corner)
        corners.removeAtIndex(corner.getIndex())
        self.updateTitles()
        self.updateOverlay()
    }
    
    func removeAnnotationFromMap() {
        
        for corner in corners {
            mapView?.removeAnnotation(corner)
        }
        
        mapView?.removeOverlay(polygon)
        
    }
    
    func lock() {
        locked = true
        insertIndex = nil;
        for corner in corners {
            mapView?.removeAnnotation(corner)
        }
    }
    
    func unlock() {
        locked = false
        for corner in corners {
            mapView?.addAnnotation(corner)
        }
    }
    
    
    func updateTitles() {
        
        for index in 0...corners.count-1 {
            corners[index].title = "Corner nr: \(index+1)"
        }
        
    }
    
    func containsPoint(point: MKMapPoint) -> Bool {
        return CGPathContainsPoint(self.polyPath, nil, CGPointMake(CGFloat(point.x), CGFloat(point.y)), false)
    }
    
    func updateOverlay() {
        
        mapView?.removeOverlay(polygon)
        var points = corners.map({MKMapPointForCoordinate($0.coordinate)})
        polygon = MKPolygon(points: &points, count: points.count)
        mapView?.addOverlay(polygon)
        
        
        var path = CGPathCreateMutable()
        
        if polygon != nil {
            for (var p=0; p < polygon!.pointCount; p++) {
                var mp = polygon!.points()[p];
                if (p == 0) {
                    CGPathMoveToPoint(path, nil, CGFloat(mp.x), CGFloat(mp.y));
                } else {
                    CGPathAddLineToPoint(path, nil, CGFloat(mp.x), CGFloat(mp.y));
                }
            }
            
            polyPath = path;
        }
        
        
        
    }
}


class Corner : NSObject, MKAnnotation  {

    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var area : Area
    
    init(coordinate: CLLocationCoordinate2D, area: Area) {
        self.coordinate = coordinate
        self.area = area
    }
    
    func getIndex () -> Int {
        return find(self.area.corners, self)!
    }
    
}